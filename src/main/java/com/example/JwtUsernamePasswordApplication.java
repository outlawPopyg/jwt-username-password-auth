package com.example;

import com.example.properties.RsaProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(RsaProperties.class)
public class JwtUsernamePasswordApplication {

    public static void main(String[] args) {
        SpringApplication.run(JwtUsernamePasswordApplication.class, args);
    }
}
